﻿using MyExceptions;
using System;

namespace MonitoringClasse
{
	class SimpleMenuUtils
	{
		//  Les constantes pour le menu
		public const int MENU_MyException = 1;
		public const int MENU_TropPetitIntException = 2;
		public const int MENU_AutreExceptionNonGeree = 3;
		public const int MENU_QUITTER = 4;

		//attribut last exception
		public Exception lastException { get; protected set; }

		/// <summary>
		/// Affiche le menu de l'application
		/// </summary>
		public SimpleMenuUtils AfficheMenu()
		{
			Console.WriteLine("\n\n----------------------------------------------------------------------");
			Console.WriteLine("--                                MENU                              --");
			Console.WriteLine("----------------------------------------------------------------------");
			Console.WriteLine("\n\nChoisissez une option...\nou faites une erreur: 0, lettre, trop petit, trop grand...\n");
			Console.WriteLine("\t{0}) --> Générer une MyExceptionBase", MENU_MyException);
			Console.WriteLine("\t{0}) --> Générer une TropPetitIntExceptionClass", MENU_TropPetitIntException);
			Console.WriteLine("\t{0}) --> Simuler l'apparition d'une exception non prévue", MENU_AutreExceptionNonGeree);
			Console.WriteLine("\t{0}) --> Quitter", MENU_QUITTER);
			Console.Write("\n\t\t\t\tVotre choix: ");
			return this;
		}

		/// <summary>
		/// Permet la saisie au clavier sécurisée d'un nombre entier
		/// </summary>
		/// <returns>L'entier saisi</returns>
		public int SaisirUnEntier()
		{
			string saisieUtilisateur = "";
			int nombre = 0;
			bool erreurSaisie = false;

			do
			{
				try
				{
					saisieUtilisateur = Console.ReadLine();
					nombre = int.Parse(saisieUtilisateur);
					erreurSaisie = false;
				}
				catch (FormatException ex)
				{
					//  Interception des exceptions d'erreur de conversion venant du int.Parse()
					Console.WriteLine("\n*****>> Merci de saisir un nombre entier...");
					erreurSaisie = true;
				}
			} while (erreurSaisie);

			return nombre;
		}

		/// <summary>
		/// Permet de saisir un nombre entier dans une fourchette donnée.
		/// </summary>
		/// <param name="min">La valeur min</param>
		/// <param name="max">La valeur max</param>
		/// <exception cref="MyExceptions.NulIntException">Le nombre saisi est égal à zéro</exception>
		/// <exception cref="MyExceptions.TropPetitIntException">Le nombre saisi est inférieur à min</exception>
		/// <exception cref="MyExceptions.TropGrandIntException">Le nombre saisi est égal supérieur à max</exception>
		/// <returns>Le nombre entier saisi</returns>
		public int SaisirUnEntierEntre(int min = int.MinValue, int max = int.MaxValue)
		{
			int nombre = 0;

			nombre = SaisirUnEntier();
			if (nombre == 0)
			{
				NulIntException ex = new NulIntException("Nb nul dans SaisirUnEntierEntre()");
				ex.Data.Add("Value", nombre);
				ex.Data.Add("Méthode", "SaisirUnEntier");
				ex.Data.Add("ErrorCode", "NulIntException");
				throw ex;
			}
			if (nombre < min)
			{
				TropPetitIntException ex = new TropPetitIntException("Nb Trop petit dans SaisirUnEntierEntre()");
				ex.Data.Add("Value", nombre);
				ex.Data.Add("Méthode", "SaisirUnEntier");
				ex.Data.Add("ErrorCode", "TropPetitIntException");
				throw ex;
			}
			else if (nombre > max)
			{
				TropGrandIntException ex = new TropGrandIntException("Nb Trop grand dans SaisirUnEntierEntre()");
				ex.Data.Add("Value", nombre);
				ex.Data.Add("Méthode", "SaisirUnEntier");
				ex.Data.Add("ErrorCode", "TropGrandIntException");
				throw ex;
			}

			return nombre;
		}

		public SimpleMenuUtils Run()
		{
			int choixUtilisateur = 0;
			bool erreurSaisie = true;
			do
			{
				try
				{
					this.AfficheMenu();
					do
					{
						try
						{
							choixUtilisateur = this.SaisirUnEntierEntre(SimpleMenuUtils.MENU_MyException, SimpleMenuUtils.MENU_QUITTER);
							erreurSaisie = false;
						}
						catch (TropPetitIntException ex)
						{
							Console.WriteLine("\n*****>> {0}, {1}.\nMerci de saisir un nombre plus grand", ex.Data["Value"], ex.Message);
						}
						catch (TropGrandIntException ex)
						{
							Console.WriteLine("\n*****>> {0}, {1}.\nMerci de saisir un nombre plus petit", ex.Data["Value"], ex.Message);
						}
					} while (erreurSaisie);

					switch (choixUtilisateur)
					{
						case MENU_MyException:
							throw new MyExceptionBase("Une MyExceptionBase prévue...");
							break;
						case MENU_TropPetitIntException:
							TropPetitIntException ex = new MyExceptions.TropPetitIntException("Nb Trop petit dans SaisirUnEntierEntre()");
							ex.Data.Add("Value", -1);
							throw ex;
							break;
						case MENU_AutreExceptionNonGeree:
							string message = "Exception non géré par l'application ==> MAUVAIS!!";
							Random hasard = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
							switch (hasard.Next(4))
							{
								case 0:
									throw new ArithmeticException(message);
									break;
								case 1:
									throw new IndexOutOfRangeException(message);
									break;
								case 2:
									throw new NullReferenceException(message); //non géré donc visual reprend la main
									break;
								case 3:
									throw new ArgumentOutOfRangeException(message);
									break;
								default:
									throw new InvalidOperationException(message);
									break;
							}
							break;
						case SimpleMenuUtils.MENU_QUITTER:
							Console.WriteLine("\n\nMerci d'avoir utilisé notre application!\nUNE TOUCHE SVP");
							break;
						default:
							Console.WriteLine("Bug! Cela ne devrait pas arriver!\nUNE TOUCHE SVP");
							Console.ReadKey();
							break;
					}
				}
				catch (MyExceptions.NulIntException ex)
				{
					Console.WriteLine("\n*****>> {0}.\nMerci de saisir un nombre différent de 0", ex.Message);
					lastException = ex;
					throw ex;
				}
				catch (MyExceptions.MyExceptionBase ex)
				{
					Console.WriteLine("\n*****>> {0}.\nGestion de l'exception {1}", ex.Message, ex.GetType());
					lastException = ex;
					throw ex;
				}
				catch (Exception ex)
                {
					Console.WriteLine("\n*****>> {0}.\nGestion de l'exception {1}", ex.Message, ex.GetType());
					DateTime dt = DateTime.Now;
					ex.Data.Add("DateTime", dt);
					lastException = ex;
					throw ex;
				}
			} while (choixUtilisateur != SimpleMenuUtils.MENU_QUITTER);

			return this;
		}
	}
}
