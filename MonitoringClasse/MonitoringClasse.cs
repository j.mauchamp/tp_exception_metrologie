﻿using System;
using System.Collections;

namespace MonitoringClasse
{
	class MonitoringClasse
	{
		static void Main(string[] args)
		{
			try{
				SimpleMenuUtils Menu = new SimpleMenuUtils();
				Menu.Run();
			}
			catch (Exception ex)
			{
				System.Text.StringBuilder message = new System.Text.StringBuilder();
				message.AppendFormat("*****>>Exception {0} non gérée dans {1}\n", ex.GetType(), ex.Source);
				message.AppendFormat("Message: {0}\n", ex.Message);
				message.AppendFormat("Pile d'appel:{0}\n", ex.StackTrace);

				Console.WriteLine("*****");
				Console.WriteLine(message);

				Console.WriteLine("*****");
				if (ex.Data != null)
				{
                    foreach (DictionaryEntry data in ex.Data)
                    {
                        Console.WriteLine("{0} = {1}", data.Key, data.Value);
                    }
                }
				Console.WriteLine("*****");
			}
			finally
			{
				Console.Write("\n\nFINIR L'APPLICATION SVP");
				Console.ReadKey();
			}
		}
	}
}
