﻿using System;

namespace MyExceptions
{
	/// <summary>
	/// Les codes d'erreurs possibles.
	/// </summary>
	public enum ErrorCode { NotExpected, SystemException, MyGeneric, TooSmall, TooBig, Zero  }

	/// <summary>
	/// Classe racine des exceptions propres à notre application.
	/// </summary>
	public class MyExceptionBase : Exception
	{
		public MyExceptionBase(string message = "")
			: base(message)
		{
			DateTime dt = DateTime.Now;
			this.Data.Add("DateTime", dt);
		}
	}

	/// <summary>
	/// Lancée pour signaler une erreur en raison d'un nombre entier trop petit
	/// </summary>
	public class TropPetitIntException : MyExceptionBase
	{
		public TropPetitIntException(string message = "")
			: base(message)
		{
		}

	}

	/// <summary>
	/// Lancée pour signaler une erreur en raison d'un nombre entier trop grand
	/// </summary>
	public class TropGrandIntException : MyExceptionBase
	{
		public TropGrandIntException(string message = "")
			: base(message)
		{
		}
	}

	/// <summary>
	/// Lancée pour signaler une erreur en raison d'un nombre entier nul
	/// </summary>
	public class NulIntException : MyExceptionBase
	{
		public NulIntException(string message = "")
			: base(message)
		{
		}
	}
}
